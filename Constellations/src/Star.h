#pragma once

#include "ofMain.h"

class Star {

public:
	void setup(float x, float y, float z, float r, float a, int hr);
	void draw();
	void drawOrbit(int x, int y, int z, int r);
	void getPosition();
	void Star::setCol(int r, int g, int b, float a);

	int orbX, orbY, orbZ, orbR;
	float ang, orbitX, orbitZ;
	int hour;
	int lineCount;
	ofVec3f Pos;

	int cR, cG, cB, cA;

private:
	

	vector <ofSpherePrimitive> orbit;

	ofSpherePrimitive star;

};

