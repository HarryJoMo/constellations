#pragma once

#include "ofMain.h"
#include "Star.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		ofFbo fbo;

		//vector<int> starx, stary;
		//int stars;

		ofShader shader;
		
		/*
		int starx[500];
		int stary[500];
		int color_r[500], color_g[500], color_b[500], color_a[500];
		int hour;
		*/
		
		double ang;

		int starBox;

		vector <ofPolyline> starLines;
		vector <int> lineCol;
		//vector <vector<Star>> constel;
		vector <Star> stars;

		ofVec3f camPos;

		ofImage star, space;

		ofCylinderPrimitive sphere;
		ofLight light, light2, light3, light4;
		ofTexture mTex;
		ofEasyCam cam;

		ofShader starTex;

		bool autoRot;
		bool debug;

		int hour;
		
		float starx[500];
		float stary[500];
		float starVx[500];
		float starVy[500];

		float star2x[500];
		float star2y[500];
		float star2Vx[500];
		float star2Vy[500];

		float star3x[500];
		float star3y[500];
		float star3Vx[500];
		float star3Vy[500];

		float star4x[500];
		float star4y[500];
		float star4Vx[500];
		float star4Vy[500];

		int color_r[500], color_g[500], color_b[500], color_a[500];
		
};
