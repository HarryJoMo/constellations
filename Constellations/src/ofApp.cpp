#include "ofApp.h"
#include <vector>
#include <iostream>

//--------------------------------------------------------------
void ofApp::setup(){

	// set the background colour to black (like the night sky)
	ofBackground(0);

	// setup for creating a 3d project
	ofDisableAlphaBlending();
	ofEnableDepthTest();

	// set the framerate to 30, this helps keeps things more consistent
	ofSetFrameRate(30);

	// enable the lights and aims them at the four sides of the skybox
	light.enable();
	light2.enable();
	light3.enable();
	light4.enable();

	light.setPosition(0,0,0);
	light2.setPosition(0,0,0);
	light3.setPosition(0,0,0);
	light4.setPosition(0,0,0);

	light.lookAt(ofVec3f(0, 0, -1350));
	light2.lookAt(ofVec3f(0, 0, 1350));

	ofPushMatrix();
	ofRotateYDeg(90);
	light3.lookAt(ofVec3f(0, 0, -1350));
	light4.lookAt(ofVec3f(0, 0, 1350));
	ofPopMatrix();

	// generate the starting positions for all of the background stars
	for (int i = 0; i < 500; i++)
	{
		// first wall star coords
		starx[i] = -1350 + rand() % 2700;
		stary[i] = -1100 + rand() % 2200;

		// second wall star coords
		star2x[i] = -1350 + rand() % 2700;
		star2y[i] = -1100 + rand() % 2200;

		// third wall star coords
		star3x[i] = -1350 + rand() % 2700;
		star3y[i] = -1100 + rand() % 2200;

		// fourth wall star coords
		star4x[i] = -1350 + rand() % 2700;
		star4y[i] = -1100 + rand() % 2200;

		// generate the colours randomly for each star
		color_r[i] = rand() % 255;
		color_g[i] = rand() % 255;
		color_b[i] = rand() % 255;
		color_a[i] = rand() % 255;

		// generate the velocity randomly for each star
		starVx[i] = ofRandomf();
		starVy[i] = ofRandomf();

		// ensure that all stars are moving in the same x direction
		if (starVx[i] < 0)
		{
			starVx[i] = starVx[i] * -1;
		};

		// ensure that all stars are moving in the same y direction
		if (starVy[i] > 0)
		{
			starVy[i] = starVy[i] * -1;
		};
	};

	// load the custom made star image to be used for background stars
	star.load("images/star.png");
	
	// set the hour, angle, autorotate, and debug
	hour = 0;
	ang = 0;
	autoRot = true;
	debug = false;

	// allocate the canvas for fbo
	int w = ofGetWidth();
	int h = ofGetHeight();
	fbo.allocate(w, h);
	
}

//--------------------------------------------------------------
void ofApp::update(){

	// set the fbo background to black
	fbo.begin();
	ofBackground(0);
	fbo.end();
	
}

//--------------------------------------------------------------
void ofApp::draw(){

	// set the hour variable to the actual hour
	hour = ofGetHours();

	// testing the fbo
	fbo.begin();
	ofSetColor(255);
	ofDrawRectangle(10, 10, 100, 100);
	fbo.end();

	// set camera in the direct centre of the space
	cam.setPosition(ofVec3f(0, 0, 0));

	// if autoRot is enabled, rotate the camera to be facing a point in the circle based on the time of day
	if (autoRot)
	{
		ang = ((hour * 15)*0.01745329252) + ((ofGetMinutes() * 0.25)*0.01745329252);

		//ang = ang + 0.00000231481; // rotate the camera through one full rotation throughout the day, not based on time of day
		//cout << ang << endl; // print the angle for debugging purposes
	};

	
	
	// calculate the coordinates of the point in a circle for the camera to look at
	double rotX = cam.getX() + cos(ang) * 30;
	double rotZ = cam.getZ() + sin(ang) * 30;

	// calculate the specific point for the camera to look at
	ofVec3f rotate = ofVec3f(rotX, cam.getY(), rotZ);
	cam.lookAt(rotate);

	// set the amount of stars generated for the background to be based on the amount of foreground constellation stars
	starBox = stars.size() * 5;

	// set the top limit for background stars per wall of skybox at 500
	if (starBox > 500)
	{
		starBox = 500;
	};

	// begin drawing in the 3d space
	cam.begin();
	
	// draw and move the stars for the skybox in the background
	for (int i = 0; i < starBox; i++)
	{

		// if the star goes past the width of the screen, reset it to 10 pixels behind the screen
		if (starx[i] > 1920)
		{
			starx[i] = -10;
		};

		// if the star goes past the height of the screen, reset it to 10 pixels behind the screen
		if (stary[i] < -800)
		{
			stary[i] = 1200;
		};

		// add the movement to the position of the stars
		starx[i] = starx[i] + starVx[i];
		stary[i] = stary[i] + starVy[i];

		//cout << stary[i] << endl;

		ofSetColor(color_r[i], color_g[i], color_b[i], color_a[i]); // set the randomly generated colours from setup
		star.draw(starx[i], stary[i], -1300, 5, 5); // draw the star image at the randomly generated positions from setup
	};

	for (int i = 0; i < starBox; i++)
	{

		// if the star goes past the width of the screen, reset it to 10 pixels behind the screen
		if (star2x[i] > 1920)
		{
			star2x[i] = -10;
		};

		// if the star goes past the height of the screen, reset it to 10 pixels behind the screen
		if (star2y[i] < -800)
		{
			star2y[i] = 1200;
		};

		// add the movement to the position of the stars
		star2x[i] = star2x[i] + starVx[i];
		star2y[i] = star2y[i] + starVy[i];

		//cout << stary[i] << endl;

		ofSetColor(color_r[i], color_g[i], color_b[i], color_a[i]); // set the randomly generated colours from setup
		star.draw(star2x[i], star2y[i], 1300, 5, 5); // draw the star image at the randomly generated positions from setup
	};

	// use push and pop to rotate the canvas and draw the other two walls
	ofPushMatrix();
	ofRotateYDeg(90);

	// draw and move the stars
	for (int i = 0; i < starBox; i++)
	{

		// if the star goes past the width of the screen, reset it to 10 pixels behind the screen
		if (star3x[i] > 1920)
		{
			star3x[i] = -10;
		};

		// if the star goes past the height of the screen, reset it to 10 pixels behind the screen
		if (star3y[i] < -800)
		{
			star3y[i] = 1200;
		};

		// add the movement to the position of the stars
		star3x[i] = star3x[i] + starVx[i];
		star3y[i] = star3y[i] + starVy[i];

		ofSetColor(color_r[i], color_g[i], color_b[i], color_a[i]); // set the randomly generated colours from setup
		star.draw(star3x[i], star3y[i], -1300, 5, 5); // draw the star image at the randomly generated positions from setup
	};

	for (int i = 0; i < starBox; i++)
	{

		// if the star goes past the width of the screen, reset it to 10 pixels behind the screen
		if (star4x[i] > 1920)
		{
			star4x[i] = -10;
		};

		// if the star goes past the height of the screen, reset it to 10 pixels behind the screen
		if (star4y[i] < -800)
		{
			star4y[i] = 1200;
		};

		// add the movement to the position of the stars
		star4x[i] = star4x[i] + starVx[i];
		star4y[i] = star4y[i] + starVy[i];

		//cout << stary[i] << endl;

		ofSetColor(color_r[i], color_g[i], color_b[i], color_a[i]); // set the randomly generated colours from setup
		star.draw(star4x[i], star4y[i], 1300, 5, 5); // draw the star image at the randomly generated positions from setup
	};

	ofPopMatrix();
	
	ofSetColor(255);

	// draw the connections between the stars for constellations
	for (int i = 0; i < stars.size(); i++)
	{

		for (int j = 0; j < stars.size(); j++)
		{
			// check that the two stars were drawn within the same hour
			if (stars[i].hour == stars[j].hour)
			{
				
				// get the positions of the two stars
				ofVec3f star1Pos = stars[i].Pos;
				ofVec3f star2Pos = stars[j].Pos;

				// check the distance between the two stars is less than 160
				if (ofDist(star1Pos.x, star1Pos.y, star1Pos.z, star2Pos.x, star2Pos.y, star2Pos.z) < 160)
				{

					// create a line
					ofPolyline line;

					// set two vertices on that line as the two star positions
					line.addVertex(star1Pos);
					line.addVertex(star2Pos);
					line.end();

					// add that line to the starLines vertex, add the colour for that line starting as black to a seperate line colour vertex
					starLines.push_back(line);
					lineCol.push_back(0);

					//stars[i].lineCount++;
					//stars[j].lineCount++;
				};


			};

		};

	};

	// colouring the stars
	for (int i = 0; i < stars.size(); i++)
	{

		// check the alpha of the stars is less than full
		if (stars[i].cA < 255)
		{

			// raise the alpha of the stars each frame
			stars[i].cA = stars[i].cA + 1;
		};

		// set the colour of each star
		ofSetColor(stars[i].cA, stars[i].cA, stars[i].cA, stars[i].cA);

		// draw each star
		stars[i].draw();
	};

	// colouring the constellation lines
	for (int i = 0; i < starLines.size(); i++)
	{

		// check the colour of the lines is less than full
		if (lineCol[i] < 255)
		{

			// raise the colour of the lines each frame
			lineCol[i]++;
		};

		// set the colour of each line
		ofSetColor(lineCol[i]);

		// draw each line
		starLines[i].draw();
	};

	// draw the orbit for debugging purposes, shows the maximum radius where the stars can spawn
	//orbit.drawOrbit(0,0,0,100);

	/*
	// drawing rectangles to calculate dimensions of the skybox for the background stars
	ofSetColor(130);

	//fbo.draw()
		fbo.bind();
	ofDrawRectangle(-1350, -1100, -1350, 2700, 2200);
	ofDrawRectangle(-1350, -1100, 1350, 2700, 2200);
	fbo.unbind();

	ofPushMatrix();
	ofRotateYDeg(90);
	fbo.bind();
	ofDrawRectangle(-1350, -1100, -1350, 2700, 2200);
	ofDrawRectangle(-1350, -1100, 1350, 2700, 2200);
	fbo.unbind();
	ofPopMatrix();
	*/

	// stop drawing into the camera
	cam.end();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

	// press the space key to create your own star
	if (key == ' ')
	{
		Star newSt;

		float angle = ((hour * 15)*0.01745329252) + ((rand() % 15)*0.01745329252);
		int radius = 1000 + (rand() % 300);
		int starY = -600 + (rand() % 1200);

		newSt.setup(0, starY, 0, radius, angle, hour);
		newSt.lineCount = 0;

		newSt.setCol(0, 0, 0, 0);

		stars.push_back(newSt);
		//newSt.setPosition(ofVec3f());
		//cout << cam.getPosition() << endl;
	};

	/*
	ofSpherePrimitive orbitMark;
	orbitMark.setRadius(5);
	orbitMark.setPosition(ofVec3f(orbitX, y, orbitZ));
	orbitMark.draw();
	*/

	// debugging tools
	if (debug) {

		// raise and lower the hour counter manually
		if (key == 'w') {

			if (hour != 23) {
				hour++;
			};

			if (hour == 23) {
				hour = 0;
			};

		};

		if (key == 's') {

			if (hour != 0) {
				hour--;
			};

			if (hour == 0) {
				hour = 23;
			};
		};

		// change the angle the camera is looking at manually
		if (key == 'e') {
			ang = ang + 0.1;
		};

		if (key == 'd') {
			ang = ang - 0.1;
		};

		// check the y value of a background star
		if (key == 'c') {
			cout << stary[1] << endl;
		};

		// turn on / off auto rotate
		if (key == 'a') {
			autoRot = !autoRot;
		};
	};
	
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
