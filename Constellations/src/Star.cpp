#include "Star.h"

//------------------------------------------------------------
void Star::setup(float x, float y, float z, float r, float a, int hr) {

	// set the main variables of the star
	orbX = x; // orbit origin x
	orbY = y; // star y level
	orbZ = z; // orbit origin z
	orbR = r; // radius of orbit
	ang = a; // angle of star in the orbit
	hour = hr; // hour the star was created
}

//------------------------------------------------------------
void Star::setCol(int r, int g, int b, float a) {

	// set the colour of the star
	cR = r;
	cG = g;
	cB = b;
	cA = a;
}

//------------------------------------------------------------
void Star::draw() {

	// use setup values to draw the star at its point in orbit
	orbitX = orbX + cos(ang) * orbR;
	orbitZ = orbZ + sin(ang) * orbR;

	// set the size of the stars radius to 5
	star.setRadius(5);

	// set the stars position
	Pos = ofVec3f(orbitX, orbY, orbitZ);
	star.setPosition(Pos);
	
	// draw the star
	star.draw();

}

//------------------------------------------------------------
void Star::getPosition() {

	// simple code to get the stars position for creating constellations
	star.getPosition();
}

//--------------------------------------------------------------
void Star::drawOrbit(int x, int y, int z, int r) {

	// draw the orbit, used for debugging as it allows you to see the radius that the stars can be spawned within

	// draw the orbit as a visible circle
	for (double i = 0; i < 11; i++) {
		int ang = i*12;
		float orbitX = x + cos(ang) * r;
		float orbitZ = z + sin(ang) * r;

		// draw the orbit in sphere markers
		ofSpherePrimitive orbitMark;
		orbitMark.setRadius(5);
		orbitMark.setPosition(ofVec3f(orbitX, y, orbitZ));
		orbitMark.draw();
		//ofDrawCircle(orbitX, orbitZ, 1);
	};
}