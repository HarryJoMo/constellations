#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	//shader.load("shader.vert", "shader.frag");

	// randomly generate star positions, velocity, and colours
	for (int i = 0; i < 500; i++)
	{
		starx[i] = (float)(rand() % 1920);
		stary[i] = (float)(rand() % 1080);
		color_r[i] = rand() % 255;
		color_g[i] = rand() % 255;
		color_b[i] = rand() % 255;
		color_a[i] = rand() % 255;
		starVx[i] = ofRandomf();
		//starVy[i] = ofRandomf();
		starVy[i] = 0.6;

		if (starVx[i] < 0)
		{
			starVx[i] = starVx[i] * -1;
		};

		if (starVy[i] > 0)
		{
			starVy[i] = starVy[i] * -1;
		};
	};

	// load star image
	star.load("images/star.png");

	ofDisableAlphaBlending();
	ofEnableDepthTest();
	light.enable();
	light.setPosition(ofVec3f(100, 100, 200));
	light.lookAt(ofVec3f(0, 0, 0));

	mesh1 = sphere.getMesh();
}

//--------------------------------------------------------------
void ofApp::update(){
	// set background for fbo
	fbo1.begin();
	ofBackground(0);
	fbo1.end();

	
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofSetColor(255);
	
	cam.begin();
	
	//fbo.begin();
	for (int i = 0; i < 200; i++)
	{
		color_a[i] = 105 + (rand() % 150); // 50 stars will flicker
	};

	// draw and move the stars
	for (int i = 0; i < 500; i++)
	{

		// if the star goes past the width of the screen, reset it to 10 pixels behind the screen
		if (starx[i] > 1920)
		{
			starx[i] = -10;
		};

		// if the star goes past the height of the screen, reset it to 10 pixels behind the screen
		if (stary[i] > 1080)
		{
			stary[i] = -10;
		};

		// add the movement to the position of the stars
		starx[i] = starx[i] + starVx[i];
		stary[i] = stary[i] + starVy[i];

		ofSetColor(color_r[i], color_g[i], color_b[i], color_a[i]); // set the randomly generated colours from setup
		star.draw(-1000 + starx[i], -500 + stary[i], -1000, 5, 5); // draw the star image at the randomly generated positions from setup
	};
	
	// draw 3d sphere, testing how a star would look with the 2d background
	mesh1.drawWireframe();
	
	cam.end();
	
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == 'a')
	{
		cout << ofRandomf() << endl;
	};
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
