#pragma once

#include "ofMain.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		//void drawFbo();
		//void customDraw3d();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		ofShader shader;
		ofImage star;
		ofFbo fbo1;

		ofSpherePrimitive sphere;

		ofEasyCam cam;
		ofLight light;

		ofTexture mTex;

		//int ecran = (ofGetWidth() / 2);

		//ofFbo Mainfbo;
		//ofFbo fbo1;
		//ofFbo fbo2;

		//vector<ofVec3f> points;

		ofMesh mesh1;

		float starx[500];
		float stary[500];
		float starVx[500];
		float starVy[500];
		
		int color_r[500], color_g[500], color_b[500], color_a[500];
};
