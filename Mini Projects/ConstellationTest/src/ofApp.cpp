#include "ofApp.h"
#include<vector>

//--------------------------------------------------------------
void ofApp::setup(){
	ofBackground(0); // make background black

	ofSetFrameRate(15); // set frame rate to 15, makes clock run slower

	minCount = 0; // initialise the minute counter to 0
	hour = 0; // initialise the hour counter to 0

	go = false; // initialise automatic generation as false
}

//--------------------------------------------------------------
void ofApp::update(){
	
}

//--------------------------------------------------------------
void ofApp::draw() {
	ofSetColor(255); // set the draw colour to white

	// draw the outer circle that all the stars are drawn within
	Star orbit;
	orbit.drawOrbit((ofGetWidth() / 2), (ofGetHeight() / 2), 200);

	if (go) {

	// run through 60 minutes, raise hour counter and reset minutes when 60 is hit
	if (minCount > 59)
	{
		minCount = 0;

		if (hour != 23) {
			hour++;
		}
		else {
			hour = 0;
		};
	};

	minCount++; // raise minute counter by 1 each frame

	cout << minCount << endl;

	// draw clock colon
	ofDrawBitmapString(":", (ofGetWidth() / 2) - 15, (ofGetHeight() / 2) - 1);

	// draw hours, add a 0 in front of hour if hour is less than 10 for consistency
	if (hour < 10) {
		ofDrawBitmapString("0", (ofGetWidth() / 2) - 30, (ofGetHeight() / 2));
		ofDrawBitmapString(hour, (ofGetWidth() / 2) - 22, (ofGetHeight() / 2));
	}
	else {
		ofDrawBitmapString(hour, (ofGetWidth() / 2) - 30, (ofGetHeight() / 2));
	};

	// draw minutes, add a 0 in front of minutes if minutes is less than 10 for consistency
	if (minCount < 10) {
		ofDrawBitmapString("0", (ofGetWidth() / 2) - 8, (ofGetHeight() / 2));
		ofDrawBitmapString(minCount, (ofGetWidth() / 2), (ofGetHeight() / 2));
	}
	else {
		ofDrawBitmapString(minCount, (ofGetWidth() / 2) - 8, (ofGetHeight() / 2));
	};

	// draw the hour markers in a circle
	int hr = 0;
	for (int i = 0; i < 24; i++)
	{

		float hrAng = ((i * 15)*0.01745329252) + ((15 * 0.01745329252) / 2);
		float hrX = (ofGetWidth() / 2) + cos(hrAng) * 90;
		float hrY = (ofGetHeight() / 2) + sin(hrAng) * 90;
		ofDrawBitmapString(i, hrX - 6, hrY + 5);
		hr++;
	};

	// draw AM or PM depending on the time
	if (hour < 12) {
		ofDrawBitmapString("AM", (ofGetWidth() / 2) + 10, ofGetHeight() / 2);
	};
	if (hour > 11) {
		ofDrawBitmapString("PM", (ofGetWidth() / 2) + 10, ofGetHeight() / 2);
	};

	// create and set up a new star each frame and push it into the stars vector
	Star newS;

	float angle = ((hour * 15)*0.01745329252) + ((rand() % 15)*0.01745329252);
	int radius = 100 + (rand() % 100);

	newS.setup((ofGetWidth() / 2), (ofGetHeight() / 2), radius, angle);
	stars.push_back(newS);
	};

	// draw all of the stars
	for (int i = 0; i < stars.size(); i++) {
		
		stars[i].draw();
		
	};
	
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

	// for debugging
	// manually create and set up a new star when f is pressed, push into the stars array
	if (key == 'f') {

		Star newS;

		float angle = ((hour * 15)*0.01745329252) + ((rand() % 15)*0.01745329252);
		int radius = 100 + (rand() % 100);

		newS.setup((ofGetWidth() / 2), (ofGetHeight() / 2), radius, angle);
		stars.push_back(newS);
	}

	// manually raise the hour counter when w is pressed, loop back to 0 once 23 is passed
	if (key == 'w') {

		if (hour != 23) {
			hour++;
		};

		if (hour == 23) {
			hour = 0;
		};

		// display hour in console for debugging
		cout << hour << endl;
	};

	// manually lower the hour counter when s is pressed, loop back to 23 once 0 is passed
	if (key == 's') {

		if (hour != 0) {
			hour--;
		};

		if (hour == 0) {
			hour = 23;
		};
	
		// display hour in console for debugging
		cout << hour << endl;
	};

	if (key == ' ') {

		// switch go bool when space key is pressed
		go = !go;

		// display go status in console for debugging
		cout << go << endl;
	};
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
