#pragma once

#include "ofMain.h"

class Star{

public:
	void setup(int x, int y, int r, float a);
	void draw();
	void drawOrbit(int x, int y, int r);

private:
	int orbX, orbY, orbR;
	float ang;
	
};