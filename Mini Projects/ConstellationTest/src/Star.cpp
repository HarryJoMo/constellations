#include "Star.h"

//------------------------------------------------------------
void Star::setup(int x, int y, int r, float a) {

	orbX = x; // orbit origin x
	orbY = y; // orbit origin y
	orbR = r; // radius of orbit
	ang = a; // angle of star in the orbit
}

//------------------------------------------------------------
void Star::draw() {
	
	// use setup values to draw the star at its point in orbit
	float orbitX = orbX + cos(ang) * orbR;
	float orbitY = orbY + sin(ang) * orbR;

	// draw the star as a circle
	ofDrawCircle(orbitX, orbitY, 2);
}

//--------------------------------------------------------------
void Star::drawOrbit(int x, int y, int r) {

	// draw the orbit as a visible circle
	for (double i = 0; i < 360; i++) {
		int ang = i;
		float orbitX = x + cos(ang) * r;
		float orbitY = y + sin(ang) * r;
		ofDrawCircle(orbitX, orbitY, 1);
	};
}