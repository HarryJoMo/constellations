#pragma once

#include "ofMain.h"
#include "Star.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		ofLight light;
		ofTexture mTex;
		ofEasyCam cam;

		//Polyline line;

		vector <ofPolyline> starLines;
		
		//vector <ofPolyline> 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0;

		int hour;
		float ang;

		Star orbit;

		vector <vector<Star>> constel;

		vector <Star> stars;

		ofSpherePrimitive star;
		ofSpherePrimitive star2;
};
