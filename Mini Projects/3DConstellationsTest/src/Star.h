#pragma once

#include "ofMain.h"

class Star {

public:
	void setup(float x, float y, float z, float r, float a, int hr);
	void draw();
	void drawOrbit(int x, int y, int z, int r);
	void getPosition();

	int orbX, orbY, orbZ, orbR;
	float ang, orbitX, orbitZ;
	int hour;
	int lineCount;
	ofVec3f Pos;

private:
	

	vector <ofSpherePrimitive> orbit;

	ofSpherePrimitive star;

	//ofSpherePrimitive orbitMark;
	//ofSpherePrimitive sphere;
};

