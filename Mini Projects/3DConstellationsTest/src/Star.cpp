#include "Star.h"

//------------------------------------------------------------
void Star::setup(float x, float y, float z, float r, float a, int hr) {

	orbX = x; // orbit origin x
	orbY = y; // star y level
	orbZ = z; // orbit origin z
	orbR = r; // radius of orbit
	ang = a; // angle of star in the orbit
	hour = hr;
}

//------------------------------------------------------------
void Star::draw() {

	orbitX = orbX + cos(ang) * orbR;
	orbitZ = orbZ + sin(ang) * orbR;

	star.setRadius(5);
	Pos = ofVec3f(orbitX, orbY, orbitZ);
	star.setPosition(Pos);
	
	star.draw();

	/*
	// use setup values to draw the star at its point in orbit
	float orbitX = orbX + cos(ang) * orbR;
	float orbitY = orbY + sin(ang) * orbR;

	ofSpherePrimitive star;

	star.setRadius(5);
	star.setPosition(ofVec3f(orbitX, 0, orbitY));
	// draw the star as a circle
	star.draw();
	//ofDrawCircle(orbitX, orbitY, 2);
	*/
}

//------------------------------------------------------------
void Star::getPosition() {
	star.getPosition();
}

//--------------------------------------------------------------
void Star::drawOrbit(int x, int y, int z, int r) {

	// draw the orbit as a visible circle
	for (double i = 0; i < 11; i++) {
		int ang = i*12;
		float orbitX = x + cos(ang) * r;
		float orbitZ = z + sin(ang) * r;

		ofSpherePrimitive orbitMark;
		orbitMark.setRadius(5);
		orbitMark.setPosition(ofVec3f(orbitX, y, orbitZ));
		orbitMark.draw();
		//ofDrawCircle(orbitX, orbitZ, 1);
	};
}