#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofBackground(0);
	ofDisableAlphaBlending();
	ofEnableDepthTest();
	light.enable();
	light.setPosition(ofVec3f(100, 100, 200));
	light.lookAt(ofVec3f(0, 0, 0));

	ang = 0;


}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){


	cam.setPosition(ofVec3f(0, 0, 0));

	if (ang > 360) {
		ang = 0;
	};
	ang = ang + 0.0001;
	double rotX = cam.getX() + cos(ang) * 30;
	double rotZ = cam.getZ() + sin(ang) * 30;

	ofVec3f rotate = ofVec3f(rotX, cam.getY(), rotZ);
	cam.lookAt(rotate);
	ofVec3f camPos = cam.getPosition();


	cam.begin();
	//star2.setPosition(ofVec3f(50, -50, 100));
	//star.draw();
	//star2.draw();

	for (int i = 0; i < stars.size(); i++)
	{

			for (int j = 0; j < stars.size(); j++)
			{

					if (stars[i].hour == stars[j].hour)
					{

						ofVec3f star1Pos = stars[i].Pos;
						ofVec3f star2Pos = stars[j].Pos;

						//cout << ofDist(star1Pos.x, star1Pos.y, star1Pos.z, star2Pos.x, star2Pos.y, star2Pos.z) << endl;

						if (ofDist(star1Pos.x, star1Pos.y, star1Pos.z, star2Pos.x, star2Pos.y, star2Pos.z) < 160)
						{

							ofPolyline line;

							line.addVertex(star1Pos);
							line.addVertex(star2Pos);
							line.end();
							starLines.push_back(line);

							stars[i].lineCount++;
							stars[j].lineCount++;
						};


					};

			};

	};

	for (int i = 0; i < stars.size(); i++)
	{
		stars[i].draw();
	};

	for (int i = 0; i < starLines.size(); i++)
	{
		starLines[i].draw();
	};

	//orbit.drawOrbit(0,0,0,100);

	cam.end();

	//cout << star.getPosition() << endl;
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == 'a')
	{
		Star newSt;

		float angle = ((hour * 15)*0.01745329252) + ((rand() % 15)*0.01745329252);
		int radius = 1000 + (rand() % 300);
		int starY = -600 + (rand() % 1200);

		newSt.setup(0, starY, 0, radius, angle, hour);
		newSt.lineCount = 0;

		stars.push_back(newSt);
		//newSt.setPosition(ofVec3f());
	};
	/*
	ofSpherePrimitive orbitMark;
	orbitMark.setRadius(5);
	orbitMark.setPosition(ofVec3f(orbitX, y, orbitZ));
	orbitMark.draw();
	*/

	if (key == 'w') {

		if (hour != 23) {
			hour++;
		};

		if (hour == 23) {
			hour = 0;
		};

	};

	if (key == 's') {

		if (hour != 0) {
			hour--;
		};

		if (hour == 0) {
			hour = 23;
		};
	};

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
